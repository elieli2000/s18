 /*Assignment:
 
 Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console. */




function Scores(myScore, totalScore)  
{
	
     let isPassed = myScore >= 75;
     let operation = myScore / totalScore * 100
     return isPassed
    };


 console.log("Passing Score is: " + '75%');
 console.log("Your Score: 90/100");
 console.log("Your Score percentage: 90%");



 let isPassingScore = Scores(90, 100);

 console.log("Boolean: " + isPassingScore);