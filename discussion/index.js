// Function able to receive data without the use of global variables or prompt()

// "name" - is a parameter
// A parameter is a variable/container that exists only in our function and is used to store information that is provided to a function when it is called/invoked
function printName(name){
	console.log("My name is " + name);
};

// Data passed into a function invocation can be received by the function
// This is what we call an argument
printName("Jungkook");
printName("Thonie");

// Data passed into the function through funtion invocation is called arguments
// The argument is then stored within a container called a parameter
function printMyAge(age){
	console.log("I am " + age);
};

printMyAge(25);
printMyAge();

// check divisibility reusably using a function with arguments and parameter
function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);

	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
};

checkDivisibilityBy8(64);
checkDivisibilityBy8(27);



/*Mini Activity*/
function favoriteSuperHero(superhero){
	console.log("My favorite SuperHero " + superhero);
};
favoriteSuperHero("Ironman");

function AnyNum(number){

	let inputnum = number % 2;

	let EvenNum = inputnum === 0;
	console.log("is " + 23 + " even number?")
	console.log(EvenNum);
	
};
AnyNum(23);


/*Mini Activity*/

function printfavoriteSongs(favfirstsong, favsecondsong, favthirdsong,
 favfourthsong, favfifthsong)
{

console.log("First: " + firstsong);
console.log("second: " + secondsong);
console.log("third: " + thirdsong);
console.log("fourth: " + fourthsong);
console.log("fifth: " + fifthsong);
};

let firstsong = "Alone";
let secondsong = "Faded";
let thirdsong = "Spectre";
let fourthsong = "Are you Lonely";
let fifthsong = "Ignite";

printfavoriteSongs(firstsong, secondsong, thirdsong, fourthsong, fifthsong);

/*Mini Activity*/

const addNumbers = (num1, num2) => {
    let add = num1 + num2;
    return add
    }
    console.log(addNumbers(2,2))

function getCircleArea(radius){
	
	return 3.1416 * (radius**2);
};

let circleArea = getCircleArea(15);
console.log('The result of getting the aread of a circle is ' + circleArea);
console.log(circleArea)